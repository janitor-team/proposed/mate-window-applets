Source: mate-window-applets
Section: utils
Priority: optional
Maintainer: Debian+Ubuntu MATE Packaging Team <debian-mate@lists.debian.org>
Uploaders: Martin Wimpress <code@flexion.org>,
           Mike Gabriel <sunweaver@debian.org>,
           Vangelis Mouhtsis <vangelis@gnugr.org>,
Build-Depends: debhelper (>= 10.3~),
               dpkg-dev (>= 1.16.1.1~),
               libglib2.0-dev (>= 2.40.0),
               libgtk-3-dev (>= 3.22.0),
               libmate-panel-applet-dev (>= 1.18),
               libwnck-3-dev,
               meson,
               valac,
Homepage: https://github.com/ubuntu-mate/mate-window-applets
Vcs-Browser: https://salsa.debian.org/debian-mate-team/mate-window-applets
Vcs-Git: https://salsa.debian.org/debian-mate-team/mate-window-applets.git
Standards-Version: 4.5.0
Rules-Requires-Root: no

Package: mate-window-buttons-applet
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         mate-window-applets-common (>= ${source:Version}),
         mate-panel,
Description: MATE Window Applets (WindowButtons Applet)
 The MATE Window Applets collection provides various applets to show
 window control elements in the MATE Panel.
 .
 This WindowButtons applet shows you the close, minimize and actions
 buttons.

Package: mate-window-title-applet
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         mate-window-applets-common (>= ${source:Version}),
         mate-panel,
Description: MATE Window Applets (WindowTitle Applet)
 The MATE Window Applets collection provides various applets to show
 window control elements in the MATE Panel.
 .
 This WindowTitle applet shows you the class, title, role, xid and pid of
 the active window.

Package: mate-window-menu-applet
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         mate-window-applets-common (>= ${source:Version}),
         mate-panel,
Description: MATE Window Applets (WindowMenu Applet)
 The MATE Window Applets collection provides various applets to show
 window control elements in the MATE Panel.
 .
 This WindowMenu applet shows you the window menu of the active window.

Package: mate-window-applets-common
Architecture: all
Depends: ${misc:Depends},
         libglib2.0-bin,
Description: MATE Window Applets (common files)
 The MATE Window Applets collection provides various applets to show
 window control elements in the MATE Panel.
 .
 This package contains the arch-independent files of the MATE Windows
 Applets.
